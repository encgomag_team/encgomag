<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */ protected $fillable=[
        'date_commande',
        'quantite',
        'status',
        'id_user',
        'id_fourniture',
    ];
    public function up(): void
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->id();
            $table->date('date_commande');
            $table->integer('quantite');
            $table->string('status');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_fourniture');
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_fourniture')->references('id')->on('fournitures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('commandes');
    }
};
