import React from 'react'
import Template from './Layout/Template'
import Card from '@/Components/Card_Profile/Card'

export default function Profile({auth}) {
  return (
    <div>
        <Template user={auth.user}>
                 <p>
                    <small style={{ fontWeight:'bolder' }}>Responsable/ Dashboard /<span style={{ color:'#C85D22' }}>profile</span></small>
                </p>
                <div className="container">
                    <div className="row d-flex justify-content-center">
                        <div className="col-lg-5">
                              <Card
                                user={auth.user}
                              />
                        </div>
                    </div>
                </div>
        </Template>
    </div>
  )
}
