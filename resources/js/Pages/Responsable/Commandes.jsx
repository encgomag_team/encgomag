import React from 'react'
import Template from './Layout/Template'

export default function Commandes({auth}) {
  return (
    <div>
        <div>
            <Template user={auth.user}>
                <p>
                    <small style={{ fontWeight:'bolder' }}>Responsable/ Dashboard /<span style={{ color:'#C85D22' }}>Liste des commandes</span></small>
                </p>
                <div className="container">
                    <div className="row d-flex justify-content-center">
                        <div className="col-lg-12">
                            <div className="table-responsive  p-2  border-0">
                                <table className='table table-hover p-2   border-white table-striped shadow'>
                                    <thead className="thead-primary">
                                        <tr>
                                            <th>ID</th>
                                            <th>Fonctionnaire</th>
                                            <th>Produit</th>
                                            <th >Date commande</th>
                                            <th className='text-center' colSpan={3}>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Gagre adama</td>
                                            <td>Ordinateur</td>
                                            <td>2024-02-04 12:03:06</td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-primary fw-bolder'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px',backgroundColor:"green",color:"white" }} className='btn fw-bold btn-sm'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-danger'>Details</button></td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Gagre adama</td>
                                            <td>Ordinateur</td>
                                            <td>2024-02-04 12:03:06</td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-primary fw-bolder'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px',backgroundColor:"green",color:"white" }} className='btn fw-bold btn-sm'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-danger'>Details</button></td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Gagre adama</td>
                                            <td>Ordinateur</td>
                                            <td>2024-02-04 12:03:06</td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-primary fw-bolder'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px',backgroundColor:"green",color:"white" }} className='btn fw-bold btn-sm'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-danger'>Details</button></td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Gagre adama</td>
                                            <td>Ordinateur</td>
                                            <td>2024-02-04 12:03:06</td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-primary fw-bolder'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px',backgroundColor:"green",color:"white" }} className='btn fw-bold btn-sm'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-danger'>Details</button></td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Gagre adama</td>
                                            <td>Ordinateur</td>
                                            <td>2024-02-04 12:03:06</td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-primary fw-bolder'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px',backgroundColor:"green",color:"white" }} className='btn fw-bold btn-sm'>Details</button></td>
                                            <td><button style={{ borderRadius:'20px' }} className='btn btn-sm btn-danger'>Details</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </Template>
        </div>
    </div>
  )
}
