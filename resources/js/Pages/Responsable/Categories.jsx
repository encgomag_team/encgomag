import React from 'react'
import Template from './Layout/Template'
import CardCategorie from '@/Components/Card_Categorie/CardCategorie'

export default function Categories({auth}) {

    const categories=[
        {
            id:1,
            nom:"Categorie fournitures",
            image:'Image'
        },
        {
            id:1,
            nom:"Categorie fournitures",
            image:'Image'
        },
        {
            id:1,
            nom:"Categorie fournitures",
            image:'Image'
        }
    ]
  return (
    <div>
        <div>
            <Template user={auth.user}>
                <p>
                    <small style={{ fontWeight:'bolder' }}>Responsable/ Dashboard /<span style={{ color:'#C85D22' }}>Categories fournitures</span></small>
                </p>
               <div className="container my-5">
                <h5 style={{ fontFamily:'Arial',fontWeight:'900',color:'#C85D22',textShadow:'0px 2px 2px black' }}>CATEGORIES</h5>
                    <div className="row d-flex justify-content-center">
                       {
                        categories.map((item)=>{
                            return (
                                <div className="col-lg-3 mb-3">
                                     <CardCategorie/>
                                </div>
                            )
                        })
                       }
                    </div>
               </div>
            </Template>
        </div>
    </div>
  )
}
