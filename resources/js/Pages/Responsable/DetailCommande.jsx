import React from 'react'
import Template from './Layout/Template'
import Card from '@/Components/Card_detail_cmd/Card'

export default function DetailCommande({auth}) {
  return (
    <div>
        <Template user={auth.user}>
                <p>
                    <small style={{ fontWeight:'bolder' }}>Responsable/ Dashboard /<span style={{ color:'#C85D22' }}>Details</span></small>
                </p>
                <div>
                    <Card/>
                </div>
        </Template>
    </div>
  )
}
