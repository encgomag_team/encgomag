import React from 'react'
import Template from './Layout/Template'
import CardProduct from '@/Components/Card_Product/CardProduct'

export default function Fournitures({auth}) {

const fournitures=[
    {
        id:1,
        titre:'Product1',
        stock:30
    },
    {
        id:1,
        titre:'Product1',
        stock:30
    },
    {
        id:1,
        titre:'Product1',
        stock:30
    },
    {
        id:1,
        titre:'Product1',
        stock:30
    },
    {
        id:1,
        titre:'Product1',
        stock:30
    },
    {
        id:1,
        titre:'Product1',
        stock:30
    },
    {
        id:1,
        titre:'Product1',
        stock:30
    },
    {
        id:1,
        titre:'Product1',
        stock:30
    }
]

  return (
    <div>
        <div>
            <Template user={auth.user}>
                <p>
                    <small style={{ fontWeight:'bolder' }}>Responsable/ Dashboard /<span style={{ color:'#C85D22' }}>Liste des fournitures</span></small>
                </p>
            <div className="row justify-content-center">
                <div className="col-sm-8">
                    <div className="input-group">
                        <input type="text" className="form-control shadow-sm btnradius" name="recherche" id="recherche" placeholder="Rechercher le type d'appareil"/>
                        <div className="input-group-append">
                             <input type="submit" className="btn  border-0 btn-primary btnradius_submit" name="envoyer" id="envoyer" value="Rechercher" />
                        </div>
                    </div>
                </div>
            </div>

               <div className="container my-5">
                    <div className="row">

                        {fournitures.map((item)=>{
                            return (
                                <div className="col-lg-3 mb-3">
                                     <CardProduct/>
                                </div>
                            )
                        })}

                    </div>
               </div>
            </Template>
        </div>
    </div>
  )
}
