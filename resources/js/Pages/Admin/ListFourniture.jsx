import React from 'react'
import Template from './Layout/Template'

export default function ListFourniture({auth}) {
  return (
    <div>
        <Template user={auth.user}>
            <div className="container">
            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className='table-responsive card p-3 border-0 shadow'>
                                            <table className='table table-bordered table-white table-hover '>
                                                <thead className=''>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Nom</th>
                                                        <th>Prenom</th>
                                                        <th>Role</th>
                                                        <th colSpan={3}>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>KAFANDO</td>
                                                        <td>Relwendé Aimé</td>
                                                        <td>fonctionnaire</td>
                                                        <td><Link href={route('detail_user')} className='btn btn-primary border-0 rounded-4'>Details</Link></td>
                                                        <td><button className='btn btn-info border-0 rounded-4'>Modifier</button></td>
                                                        <td><button className='btn btn-danger border-0 rounded-4'>Supprimer</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>KAFANDO</td>
                                                        <td>Relwendé Aimé</td>
                                                        <td>fonctionnaire</td>
                                                        <td><Link href={route('detail_user')} className='btn btn-primary border-0 rounded-4'>Details</Link></td>
                                                        <td><button className='btn btn-info border-0 rounded-4'>Modifier</button></td>
                                                        <td><button className='btn btn-danger border-0 rounded-4'>Supprimer</button></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>
        </Template>
    </div>
  )
}
