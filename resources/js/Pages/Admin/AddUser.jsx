import React from "react";
import ADD_USER_IMAGE from '../../Images/adduser.png'
import Template from "./Layout/Template";

export default function AddUser({ auth }) {
    return (
        <div>
            <Template user={auth.user}>
                <p>
                    <small style={{ fontWeight: "bolder" }}>
                        Admin/ Dashboard /
                        <span style={{ color: "green" }}>
                            Ajout d'un utilisateur
                        </span>
                    </small>
                </p>
                <div className="container ajout">
                    <div className="row d-flex justify-content-center">
                        <div className="col-lg-3">
                            <div className="image_user">
                                <img className="img-fluid" src={ADD_USER_IMAGE} alt="add_user" />
                            </div>
                        </div>
                        <div className="col-lg-5">
                            <form
                                className="card p-3 shadow-sm border-0 rounded-4"
                                action=""
                            >
                                <h5 style={{ backgroundColor:'#F0F8FF',fontFamily:'Arial black' }} className="text-center p-2">Ajout d'un utilisateur</h5>
                                <p>
                                    <label htmlFor="nom">
                                        Nom de l'utilisateur
                                    </label>
                                    <input
                                        type="text"
                                        name="nom"
                                        id="nom"
                                        className="form-control rounded-4 bg-light"
                                    />
                                </p>
                                <p>
                                    <label htmlFor="prenom">
                                        Prénom de l'utilisateur
                                    </label>
                                    <input
                                        type="text"
                                        name="prenom"
                                        id="prenom"
                                        className="form-control rounded-4 bg-light"
                                    />
                                </p>
                                <p>
                                    <label htmlFor="email">Adresse email</label>
                                    <input
                                        type="email"
                                        name="email"
                                        id="email"
                                        className="form-control rounded-4 bg-light"
                                    />
                                </p>
                                <p>
                                    <label htmlFor="cin">Numéro CIN</label>
                                    <input
                                        type="text"
                                        name="cin"
                                        id="cin"
                                        className="form-control rounded-4 bg-light"
                                    />
                                </p>
                                <p>
                                    <label htmlFor="role">Rôle</label>
                                    <select
                                        name="role"
                                        id="role"
                                        className="form-control rounded-4 bg-light"
                                    >
                                        <option value="admin">Admin</option>
                                        <option value="responsable">
                                            Responsable
                                        </option>
                                        <option value="fonctionnaire">
                                            Fonctionnaire
                                        </option>
                                    </select>
                                </p>
                                <p>
                                    <label htmlFor="password">
                                        Mot de passe
                                    </label>
                                    <input
                                        type="password"
                                        name="password"
                                        id="password"
                                        className="form-control rounded-4 bg-light"
                                    />
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </Template>
        </div>
    );
}
