import React from 'react';
import './Layout/Template.css';
import Template from './Layout/Template';

export default function EditUser({auth}) {
  return (
    <div className=" edit_user">
        <Template user={auth.user}>
        <p>
                    <small style={{ fontWeight:'bolder' }}>Admin/ Dashboard /<span style={{ color:'#0B9A22' }}>Edit user</span></small>
                </p>

      <div className="row justify-content-center">
        <div className="col-lg-6 py-3">
          <form className="border card border-0  p-4 shadow radiusform " action="">
          <h4 className="font-weight-bold fw-extrabold">Modifier les informations</h4>
            <div className="form-group">
             <p>
                <input type="text" className="form-control bginput" id="nom" name="nom" placeholder='nom'/>
             </p>

            </div>
            <div className="form-group">
             <p>
                <input type="text" className="form-control bginput"  id="prenom" name="prenom" placeholder='prenom' />
             </p>
            </div>
            <div className="form-group">
             <p>
                <input type="email" className="form-control bginput" id="prenom" name="prenom" placeholder='email' />
             </p>
            </div>
            <div className="form-group">
             <p>
                <input type="number" className="form-control bginput" id="prenom" name="prenom" placeholder='mobile' />
             </p>
            </div>
            <div>
                <p >
                    <div className='form-group text-center'>
                        <input value={"Enregistrer les modifications"} type="submit" className="btn border-0 fw-bolder btn-primary shadow col-8 btnvalider  " name="enregistrer" id="enregistrer" />
                    </div>
                </p>
            </div>
          </form>
        </div>
      </div>
        </Template>
    </div>
  );
}
