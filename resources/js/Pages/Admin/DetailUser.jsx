import React from 'react'
import Template from './Layout/Template'
import { Link } from '@inertiajs/react'

export default function DetailUser({auth}) {
  return (
    <div>
        <Template user={auth.user}>
            <div>
                <div className="container details_user my-4">
                    <div className="row d-flex justify-content-center">
                        <div className="col-lg-7">
                            <div className="card rounded-3 p-3 shadow-sm border-0">
                                <div className="d-flex">
                                    <div className="image_detail_user p-2">
                                        <img height={100} width={100} src="https://www.capsoft.com.bo/wp-content/uploads/2021/04/van-miguel.png" alt="" />
                                    </div>
                                    <div className="infos my-4">
                                        <h5 style={{ fontWeight:'900' }}>Aimé KAFANDO</h5>
                                        <h6 style={{ fontStyle:'italic' }}><small>Date de creation de compte: 03/03/2024</small></h6>
                                    </div>
                                </div>
                                <div>
                                    <ul className="list-group border-0 px-5 mx-3">
                                        <li className="list-group-item border-0 mx-2 my-1"><strong>Nom :</strong></li>
                                        <li className="list-group-item border-0 mx-2 my-1"><strong>Prénom :</strong></li>
                                        <li className="list-group-item border-0 mx-2 my-1"><strong>Email :</strong></li>
                                        <li className="list-group-item border-0 mx-2 my-1"><strong>CIN :</strong></li>
                                        <li className="list-group-item border-0 mx-2 my-1"><strong>Role :</strong></li>
                                    </ul>
                                    <p className='text-center my-2'>
                                        <Link href={route('users')} className='btn btn-primary mx-2'>Retour</Link>
                                        <button className='btn btn-danger mx-2'>Supprimer cet utilisateur</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Template>
    </div>
  )
}
