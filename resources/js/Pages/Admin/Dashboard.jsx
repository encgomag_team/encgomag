import { Link } from '@inertiajs/react';
import React from 'react';
import Template from './Layout/Template';
import Card from '@/Components/Card_dashboard/Card';

export default function Dashboard({auth}) {

    const Cart_items=[
        {
            id:1,
            titre:'Commandes en attentes',
            image:'https://thumbs.dreamstime.com/b/colorful-crayons-school-supplies-37613714.jpg',
            color:{backgroundColor:'#793D3D'},
            nombre:40,
            route:'commandes'
        },
        {
            id:2,
            titre:'Nombre de produits',
            image:'https://thumbs.dreamstime.com/b/colorful-crayons-school-supplies-37613714.jpg',
            color:{backgroundColor:'#4B541F'},
            nombre:45,
            route:'fournitures'
        },
        {
            id:2,
            titre:'Nombre de produits',
            image:'https://thumbs.dreamstime.com/b/colorful-crayons-school-supplies-37613714.jpg',
            color:{backgroundColor:'#4B541F'},
            nombre:45,
            route:'fournitures'
        },
        {
            id:1,
            titre:'Commandes en attentes',
            image:'https://thumbs.dreamstime.com/b/colorful-crayons-school-supplies-37613714.jpg',
            color:{backgroundColor:'#793D3D'},
            nombre:40,
            route:'commandes'
        },
    ]


  return (
    <div>

        <div>
            <Template user={auth.user}>
               <p>
                    <small style={{ fontWeight:'bolder' }}>Admin/ Dashboard /<span style={{ color:'green' }}>Tableau de bord</span></small>
                </p>
            <div className="container">
                <div className="row">
                    {
                    Cart_items.map((item)=>{
                        return (
                            <div className="col-lg-5 mb-3">
                                <Card
                                    titre={item.titre}
                                    nombre={item.nombre}
                                    image={item.image}
                                    color={item.color}
                                    route={item.route}
                                />
                            </div>
                        )
                    })
                    }
                </div>
            </div>
            </Template>
        </div>
    </div>
  )
}
