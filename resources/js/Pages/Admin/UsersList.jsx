import React from 'react'
import Template from './Layout/Template'
import { Link } from '@inertiajs/react'

export default function UsersList({auth}) {
  return (
    <div>
        <Template user={auth.user}>
                <p>
                    <small style={{ fontWeight:'bolder' }}>Admin/ Dashboard /<span style={{ color:'green' }}>Users/list</span></small>
                </p>
                <div className='container  justify-content-center'>
                    <div className='row'>
                        <div className='col-lg-6'>
                            <form>
                                <p className='d-flex'>
                                    <input
                                        type="search"
                                        name=""
                                        id=""
                                        className='form-control border-0 shadow-sm rounded-4'
                                        placeholder='Rechercher un utilisa...'
                                    />
                                    <button className='btn btn-primary'>Rechercher</button>
                                </p>
                            </form>
                        </div>
                        <div className='col-lg-6'>
                            <Link href={route('ajouter_user')} className='btn btn-success text-white fw-bolder'>AJouter un utilisateur</Link>
                        </div>
                        <div>
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className='table-responsive card p-3 border-0 shadow'>
                                            <table className='table table-bordered table-white table-hover '>
                                                <thead className=''>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Nom</th>
                                                        <th>Prenom</th>
                                                        <th>Role</th>
                                                        <th colSpan={3}>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>KAFANDO</td>
                                                        <td>Relwendé Aimé</td>
                                                        <td>fonctionnaire</td>
                                                        <td><Link href={route('detail_user')} className='btn btn-primary border-0 rounded-4'>Details</Link></td>
                                                        <td><button className='btn btn-info border-0 rounded-4'>Modifier</button></td>
                                                        <td><button className='btn btn-danger border-0 rounded-4'>Supprimer</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>KAFANDO</td>
                                                        <td>Relwendé Aimé</td>
                                                        <td>fonctionnaire</td>
                                                        <td><Link href={route('detail_user')} className='btn btn-primary border-0 rounded-4'>Details</Link></td>
                                                        <td><button className='btn btn-info border-0 rounded-4'>Modifier</button></td>
                                                        <td><button className='btn btn-danger border-0 rounded-4'>Supprimer</button></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </Template>
    </div>
  )
}
