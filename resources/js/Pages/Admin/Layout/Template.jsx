import React from 'react'
import './Template.css';
import { Link } from '@inertiajs/react';
import LOGO from '../../../Images/logo-no-background 6.png'

export default function Template(props) {

  const handleSidebarToggle = () => {
      const sidebar = document.getElementById('sidebar');
      sidebar.classList.toggle('show');
  };

return (
<div>
    <div>
        <nav class="navbar navbar-expand-lg navbar-light navbar-dash ">
            <div class="container-fluid">
                <a class="navbar-brand" href="#"><img height={30} width={150} src={LOGO} alt="ENCGOMAG_LOGO" /></a>
                <button style={{ backgroundColor:'black',color:"white",borderRadius:"40px" }} className="btn " id="sidebarToggle" onClick={handleSidebarToggle}><i
                        class="fa fa-solid fa-bars"></i></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0"></ul>
                </div>
                <div class="dropdown  text-light mx-5 ">
                    <a class="btn mx-2  dropdown-toggle" type="button" id="responsiveDropdown"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa mx-2 fa-solid fa-user"></i>
                        <small>{props.user.name}</small>
                    </a>
                    <ul class="dropdown-menu border-0 shadow menu-deroulante" aria-labelledby="responsiveDropdown">
                        <li><Link class="dropdown-item py-2" href="#"><i class="fa mx-2 fa-solid fa-user"></i> Profile</Link></li>
                        <li><Link method='post' href={route('logout_traitement')} class="dropdown-item py-2"><i class="fa fa-solid mx-2 fa-arrow-left"></i> Deconnexion</Link></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="sidebar show" id="sidebar">
            <ul class="nav flex-column">
                <li class="nav-item mx-2 p-2">
                    <h6 className='p-3'><small><i style={{ fontSize:"20px",color:"green" }} class="fa mx-2 fa-solid fa-circle"></i> {props.user.role}</small></h6>
                </li>
                <li class="nav-item mx-2 p-2">
                    <a class="nav-link px-3 active" href="#"><i class="fa mx-2 fa-solid fa-table"></i> Dashboard</a>
                </li>
                <li class="nav-item mx-2 p-2">
                    <Link class="nav-link px-3" href={route('users')}><i class="fa mx-2 fa-solid fa-table"></i> Gestion  utlisateurs</Link>
                </li>
                <li class="nav-item mx-2 p-2">
                    <a class="nav-link px-3" href="#"><i class="fa mx-2 fa-solid fa-table"></i> Gestion fournitures</a>
                </li>
                <li class="nav-item mx-2 p-2">
                    <a class="nav-link px-3" href="#"><i class="fa mx-2 fa-solid fa-table"></i> Gestion Categories</a>
                </li>
                <li class="nav-item mx-2 p-2">
                    <a class="nav-link px-3" href="#"><i class="fa mx-2 fa-solid fa-user"></i> Profile</a>
                </li>
                <li class="nav-item mx-2 p-2">
                    <Link class="nav-link px-3" method='post' href={ route('logout_traitement') }><i class="fa fa-solid mx-2 fa-arrow-left"></i> Deconnexion</Link>
                </li>
            </ul>
        </div>

        <main class="container-fluid py-5 main-content">
            <div class="row  py-3">
                <div class="col">


                        {/**Nos composants vont tourner autour de ca */}
                        {props.children}


                </div>
            </div>
        </main>
    </div>

</div>
)
}
