import React from 'react'
import Header from './HeaderFonct/Header'
import CardProduct from '@/Components/Card_product_fonct/CardProduct'


export default function ListFournitures({auth}) {
return (
<div>
    <Header user={auth.user}>
        <div className="container my-5 mx-3">
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-lg-5">
                        <form action="">
                            <p className='d-flex'>
                                <input
                                    type="text"
                                    placeholder='Rechercher un prouit...'
                                    className='form-control border-0 shadow-sm rounded-5'
                                />
                                <button className='btn fw-bolder rounded-5 btn-primary'>Rechercher</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
            <div className="row my-3 px-5">
                <div className="col-lg-3 mb-3">
                    <CardProduct />
                </div>
                <div className="col-lg-3 mb-3">
                    <CardProduct />
                </div>
                <div className="col-lg-3 mb-3">
                    <CardProduct />
                </div>
                <div className="col-lg-3 mb-3">
                    <CardProduct />
                </div>
            </div>
        </div>
    </Header>
</div>
)
}
