import { Link } from '@inertiajs/react'
import React from 'react'
import IMAGE from '../../Images/fournitures.jpeg';
import Header from './HeaderFonct/Header'
import CardCategorie from '@/Components/Card_Categorie/CardCategorie'
import Footer from '@/Components/Footer/Footer'
import IMAGE_BOOKING from '../../Images/passer_cmd-removebg-preview.png'
import CartFonct from '@/Components/Card_fonct/CartFonct'

export default function Dashboard({auth}) {

    const items=[
        {
            id:1,
            titre:'Faire une demande',
            url:'categories_fonct',
            image:IMAGE_BOOKING,
            color:{backgroundColor:'#AFFCAD'}

        },
        {
            id:2,
            titre:'Suivre ma demande',
            url:'suivre_demande',
            image:'https://www.french-office.fr/img/services-preparation-de-commandes.png',
            color:{backgroundColor:'#D8EDFF'}

        }
    ];

  return (
    <div className='font'>
            <Header user={auth.user}>

                <div className="thumbnail p-4">
                    <div className="container">
                       <h2 className='text-center text-white fw-bolder'>Bienvenue {auth.user.name}</h2>
                       <p className='text-center'>
                        <small style={{ color:'white' }}>Vous pouvez des a present passer une demande de fournitures <br /> et suivre l'etat de cette demande</small>
                       </p>
                    </div>
                </div>
                <div className="container my-5">
                    {/* <h6 className='text-center'>Que souhaitez vous faire ?</h6> */}
                   <div className="row d-flex my-4 justify-content-center">
                   {
                        items.map((item)=>{
                            return(
                                <div key={item.id} className="col-lg-2">
                                    <CartFonct
                                        titre={item.titre}
                                        url={item.url}
                                        image={item.image}
                                        color={item.color}
                                    />
                                </div>
                            )
                        })
                    }
                   </div>
                </div>
            </Header>

    </div>
  )
}
