import React from 'react'
import Header from './HeaderFonct/Header'

export default function SuivreDemande({auth}) {
return (
<div>
    <Header user={auth.user}>
        <div className="container my-5">
            <div className="row justify-content-center">
                <div className="col-md-12">
                    {/* <div className="p-3 border-0 shadow-sm table-responsive card">
                        <table className="table table-striped table-bordered table-hover">
                            <thead className="text-center">
                                <tr className="bg-dark text-light ">
                                    <th>ID</th>
                                    <th>Demande</th>
                                    <th>Catégories</th>
                                    <th>Status</th>
                                    <th colSpan={2}>Actions</th>
                                </tr>
                            </thead>
                            <tbody className="text-center ">

                                <tr className="bg-light">
                                    <td>5</td>
                                    <td>Tableau</td>
                                    <td>Meuble</td>
                                    <td>
                                        <div className="border rounded bg-success text-dark">Validé</div>
                                    </td>
                                    <td>
                                        <button>Details</button>
                                        <button>Annuler</button>
                                    </td>
                                </tr>

                                <tr className="bg-light">
                                    <td>5</td>
                                    <td>Tableau</td>
                                    <td>Meuble</td>
                                    <td>
                                        <div className="border rounded bg-success text-dark">Validé</div>
                                    </td>
                                    <td>
                                        <button>Details</button>
                                        <button>Annuler</button>
                                    </td>
                                </tr>

                                <tr className="bg-light">
                                    <td>5</td>
                                    <td>Tableau</td>
                                    <td>Meuble</td>
                                    <td>
                                        <div className="border rounded bg-success text-dark">Validé</div>
                                    </td>
                                    <td>
                                        <button>Details</button>
                                        <button>Annuler</button>
                                    </td>
                                </tr>

                                <tr className="bg-light">
                                    <td>5</td>
                                    <td>Tableau</td>
                                    <td>Meuble</td>
                                    <td>
                                        <div className="border rounded bg-success text-dark">Validé</div>
                                    </td>
                                    <td>
                                        <button>Details</button>
                                        <button>Annuler</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> */}
                    <div className="container">
                        <div className="row d-flex justify-content-center">
                            <h6 className='p-2 text-center cat'>Vous n'avez pas encore de demande</h6>
                            <div className="col-lg-4">
                                <div className="image_vide">
                                    <img className='img-fluid'
                                        src="https://www.wictory.fr/wp-content/uploads/2019/11/slide-ecommerce1.png"
                                        alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Header>
</div>
)
}
