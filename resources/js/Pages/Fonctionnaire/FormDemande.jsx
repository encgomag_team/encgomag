import React from 'react'
import Header from './HeaderFonct/Header'

export default function FormDemande({auth}) {



  return (
    <div>
        <Header user={auth.user}>
            <div className="container my-5">
                <h6 className='cat text-center p-3'>Faire une demande du fourniture</h6>
                <div className="row d-flex justify-content-center">
                    <div className="col-lg-4">
                        <form className='card rounded-4 p-3 shadow border-0' action="">
                            <p>
                                <label style={{ fontWeight:'bold' }}>Quantité souhaitée</label>
                                <input
                                    type="number"
                                    name=""
                                    max={100}
                                    min={1}
                                    id=""
                                    className='form-control'
                                />
                            </p>
                            <p>
                            <label style={{ fontWeight:'bold' }}>Service concerné</label>
                                <input
                                    type="text"
                                    name=""
                                    id=""
                                    className='form-control'
                                />
                            </p>
                            <p className='text-center'>
                                <button className='btn btn-success'>Sumettre ma demande</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </Header>
    </div>
  )
}

