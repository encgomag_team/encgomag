import React from 'react'
import Header from './HeaderFonct/Header'
import fourniture from '../../Images/fournitures.jpeg'
import CardCategorie from '@/Components/Card_Categorie/CardCategorie'
import { Link } from '@inertiajs/react'

export default function CategoriesFonct({auth}) {
return (
<div>
    <Header user={auth.user}>
        <div className=" p-4">
            <div className="container d-flex justify-content-center">
                <h5 className='text-center cat p-2 text-black fw-bolder'>Quelle catégorie de fourniture recherchez vous?
                </h5>
            </div>
        </div>
        <div className="container my-4 ">
            <div className="row d-flex justify-content-center">
                <div className="col-lg-2 mb-3">
                    <Link className='categorie' href={route('liste_fournitures_fonctionnaire')}>
                        <div className="card border-0 rounded-4 p-4 brradius shadow">
                            <img src={fourniture} className="card-img-top" alt=" 1" />
                            <div className="card-body ">
                                <Link className='btn btn-dark  btnnoir' href={route('liste_fournitures_fonctionnaire')}>
                                <small>Electroniques</small></Link>
                            </div>
                        </div>
                    </Link>
                </div>

                <div className="col-lg-2 mb-3">
                <Link className='categorie' href={route('liste_fournitures_fonctionnaire')}>
                        <div className="card border-0 rounded-4 p-4 brradius shadow">
                            <img src={fourniture} className="card-img-top" alt=" 1" />
                            <div className="card-body ">
                                <Link className='btn btn-dark  btnnoir' href={route('liste_fournitures_fonctionnaire')}>
                                <small>Electroniques</small></Link>
                            </div>
                        </div>
                    </Link>
                </div>

                <div className="col-lg-2 mb-3">
                <Link className='categorie' href={route('liste_fournitures_fonctionnaire')}>
                        <div className="card border-0 rounded-4 p-4 brradius shadow">
                            <img src={fourniture} className="card-img-top" alt=" 1" />
                            <div className="card-body ">
                                <Link className='btn btn-dark  btnnoir' href={route('liste_fournitures_fonctionnaire')}>
                                <small>Electroniques</small></Link>
                            </div>
                        </div>
                    </Link>
                </div>

            </div>
        </div>
    </Header>
</div>
)
}
