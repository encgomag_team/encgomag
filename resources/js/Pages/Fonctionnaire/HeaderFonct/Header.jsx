import React from 'react'
import './Header.css';
import LOGO_HEADER from '../../../Images/logo-no-background 6.png'
import { Link } from '@inertiajs/react';

export default function Header(props) {
return (
<div className='fonctionnaire_header'>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
            <a className="navbar-brand" href="/dashboard"><img src={LOGO_HEADER} height={30} width={150} alt="" /></a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="mx-auto mb-2 navbar-nav mb-lg-0">
                    <li className="nav-item">
                        <Link href={route('categories_fonct')} className="nav-link active" aria-current="page"><button className='btn button1 fw-bolder rounded-4'>Faire ma
                                demande</button></Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" href={route('fonctionnaire_suivie')}><button className='btn button2 fw-bolder rounded-4'>Suivre ma
                                demande</button></Link>
                    </li>


                    {/* <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>
                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a className="dropdown-item" href="#">Action</a></li>
                            <li><a className="dropdown-item" href="#">Another action</a></li>
                            <li>
                                <hr className="dropdown-divider">
                                </hr>
                            </li>
                            <li><a className="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li> */}

                </ul>
                <div class="dropdown  text-light mx-5 ">
                    <a class="btn mx-2 text-white  dropdown-toggle" type="button" id="responsiveDropdown"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa mx-2 fa-solid fa-user"></i>
                        <small>{props.user.name}</small>
                    </a>
                    <ul class="dropdown-menu border-0 shadow menu-deroulante" aria-labelledby="responsiveDropdown">
                        <li>
                            <Link class="dropdown-item py-2" href="#"><i class="fa mx-2 fa-solid fa-user"></i>
                            Profile</Link>
                        </li>
                        <li>
                            <Link method='post' href={route('logout_traitement')} class="dropdown-item py-2"><i
                                class="fa fa-solid mx-2 fa-arrow-left"></i> Deconnexion</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    {props.children}
</div>
)
}
