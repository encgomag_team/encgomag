import React from 'react'
import ERROR_404_IMAGE from '../../Images/404.png';
import './Error.css';
import { Link } from '@inertiajs/react';

export default function Error() {
  return (
    <div className='error'>
        <div className="d-flex justify-content-center my-4">
            <div className="image">
                <img src={ERROR_404_IMAGE} height={270} width={420} alt="" />
            </div>
        </div>
        <div>
             <h1 className='text-center'>404</h1>
             <h6 style={{ color:"black" }} className='text-center'>Nous ne trouvons pas la page que vous recherchez !</h6><br />
             <p className='text-center'>
                <Link href='/' className='btn back'>Retour à l'acceuil</Link>
             </p>
        </div>
    </div>
  )
}
