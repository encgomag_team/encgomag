import React from 'react';
import { useForm } from '@inertiajs/react';
import HeaderPublic from '../../Components/HeaderPublic/HeaderPublic';
import LOGO from '../../Images/logo_encg.png';
import LOGO_COULEUR from "../../Images/logo_couleur.png";
import Loading from '@/Components/Footer/Loading';

export default function Login() {
    const { data, setData, post, processing, errors } = useForm({
        email: '',
        password: '',
    });

    const submit = (e) => {
        e.preventDefault();
        post(route('login_traitement'));
    };

    // Vérifier s'il y a des erreurs
    const hasErrors = Object.keys(errors).length > 0;

    return (
        <div className='login'>
            {/* <HeaderPublic/> */}

            <div className='d-flex justify-content-center'>
                <div className="logo my-4">
                    <img height={140} width={130} src={LOGO} alt="LOGO" />
                </div>
            </div>
            {errors.status && <small style={{ color:"red" }} className="error">{errors.status}</small>}
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-lg-4">
                        <form className={`card animate__animated ${hasErrors ? 'animate__shakeX' : 'animate__fadeInUp'} p-3 shadow border-0`} onSubmit={submit}>
                            <div className='d-flex justify-content-center py-2'>
                                <img src={LOGO_COULEUR} alt="ENCGOMAG" height={30} width={160} />
                            </div>
                            <p>
                                <label style={{ fontFamily:'Arial black',fontSize:'13px' }}>Email</label>
                                <input
                                    id="email"
                                    type="email"
                                    className={`${hasErrors ? 'form-control border-danger' :'form-control'}`}
                                    name="email"
                                    placeholder='example@encgo.com'
                                    value={data.email}
                                    onChange={(e) => setData('email', e.target.value)}
                                />
                                {errors.email && <small style={{ color:"red" }} className="error">{errors.email}</small>}
                            </p>
                            <p>
                                <label  style={{ fontFamily:'Arial black',fontSize:'13px' }}>Mot  de passe</label>
                                <input
                                    id="password"
                                    type="password"
                                    name="password"
                                    placeholder='******'
                                    value={data.password}
                                    className="form-control"
                                    onChange={(e) => setData('password', e.target.value)}
                                />
                                {errors.password && <small style={{ color:"red" }} className="error">{errors.password}</small>}
                            </p>
                            <hr/>
                            <p className='mx-3'>
                                <button disabled={processing} type='submit' className='log btn'>
                                    {processing ? <Loading/> : 'Connexion'}
                                </button>
                            </p>
                        </form>
                    </div>
                    <br /> <br />
                </div>
            </div>
        </div>
    );
}
