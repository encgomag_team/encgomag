import { Link } from '@inertiajs/react'
import React from 'react'

export default function Forbidden() {
  return (
    <div>
        <h1>Acces interdite !</h1>
        <h6>Vous n'etes pas autorisé a acceder a cete page</h6>
        <Link href={route('back')}>Retour</Link>
    </div>
  )
}
