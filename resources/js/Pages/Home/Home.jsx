import React, { useEffect, useState } from 'react';
import './Home.css';
import HeaderPublic from '../../Components/HeaderPublic/HeaderPublic';
import Logo from '../../Images/logo_encg.png';
import Footer from '../../Components/Footer/Footer';
import { Link } from '@inertiajs/react';

export default function Home() {
    const [text, setText] = useState('');
    const originalText = "le magasin de l'ecole National de Commerce et de gestion !";

    useEffect(() => {
        let currentIndex = 0;
        const interval = setInterval(() => {
            if (currentIndex <= originalText.length) {
                setText(originalText.substring(0, currentIndex));
                currentIndex++;
            } else {
                clearInterval(interval);
            }
        }, 50);
        return () => clearInterval(interval);
    }, []);

    return (
        <div className='home'>
            <div className=''>
                <HeaderPublic />
                <div className='d-flex justify-content-center my-5'>
                    <div className='carre animate__animated animate__fadeInUp'>
                        <div className="image">
                            <img src={Logo} alt="Logo Encg" height={200} width={180} />
                        </div>
                    </div>
                </div>
                <div className='d-flex justify-content-center'>
                    <div className='rectangle mx-3 '>
                        <div className="caree px-4 my-3">
                            <h3 style={{ fontWeight: 'bolder' }} className='p-2 text-center'>Bienvenu sur ENCGOMAG</h3>
                            <p className='text-center animate__fadeInRight'>
                                <small className='phrase '>{text}</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="d-flex my-3 justify-content-center">
                    <div className="bouttons animate__animated animate__fadeInUp">
                        <button className='about mx-2 btn'>Apropos</button>
                        <Link href='/user/login' className='log mx-2 btn'>Se connecter</Link>
                    </div>
                </div>
            </div> <br /> <hr/>
            <Footer />
        </div>
    )
}
