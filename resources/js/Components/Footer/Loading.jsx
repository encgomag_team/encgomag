import React from 'react'

export default function Loading() {
return (
<div>
    <div style={{ height:'17px',width:'17px' }}  class="spinner-border" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
</div>
)
}
