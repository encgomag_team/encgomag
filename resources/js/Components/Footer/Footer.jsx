import React from 'react'
import './Footer.css';

export default function Footer() {
  return (
    <div>
        <div className='footer p-3'>
            <p className='text-center'><small>2024 @encgomag Copyright tous droit reservés</small></p>
        </div>
    </div>
  )
}
