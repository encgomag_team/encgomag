import React from 'react'
import './Card.css';

export default function Card() {
  return (
    <div>
        <div className='card details rounded-4 border-0 p-3 shadow-sm'>
            <h4 style={{ fontWeight:'bolder' }}>Details sur la commande</h4>
            <div className="container">
                <div className="row">
                    <div className="col-lg-4">
                       <div className="image">
                            <img className='img-fluid' src="https://media.ldlc.com/ld/products/00/03/56/45/LD0003564524_2_0003622067_0003928108_0004065985_0004249657.jpg" alt="" />
                           <p className='text-center'>
                           <span class="badge rounded-pill bg-success">Produit en stock</span>
                           </p>
                       </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="container">
                            <div>
                                <ul className="list-group">
                                    <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Produit commandé :</strong></li>
                                    <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Catégorie du produit</strong></li>
                                    <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Quantité :</strong></li>
                                    <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Date de commande :</strong></li>
                                    <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Nom du fonctionnaire</strong></li>
                                </ul>
                                <hr />
                                <div className='d-flex justify-content-center'>
                                    <button className='btn mx-3 btn-success'>Valider</button>
                                    <button className='btn mx-3 btn-danger'>Rejeter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}
