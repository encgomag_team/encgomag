import React from 'react'
import './CardFoct.css';
import { Link } from '@inertiajs/react';

export default function CartFonct(props) {
  return (
    <div>
        <Link href='/fonctionnaire/fournitures/categorie' style={{ textDecoration:'none' }} className='carttt'>
        <div style={props.color} className="card fffff p-3 border-0 rounded-4 ">
           <h5  style={{ color:'#C85D22',fontFamily:'Arial black',textShadow:'0px 1px 1px black' }} className='text-center'>{props.titre}</h5>
           <div className="image">
                <img className='img-fluid' src={props.image} alt="image" />
           </div>
        </div>
        </Link>
    </div>
  )
}
