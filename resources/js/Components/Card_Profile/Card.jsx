import React from 'react'
import './Card.css';
import AVATAR_PROFILE from '../../Images/Profile-Avatar-PNG.png';

export default function Card(props) {
  return (
    <div>
        <div className="card rounded-4 infos p-3 shadow border-0">
            <div className="image d-flex justify-content-center">
                <img height={100} width={100} src={AVATAR_PROFILE} alt="Image avatar profile" />
            </div>
            <div>
                <div>
                    <ul className="list-group">
                        <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Nom : {props.user.name}</strong></li>
                        <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Prénom :{props.user.prenom}</strong></li>
                        <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Adresse email : {props.user.email}</strong></li>
                        <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>CIN : {props.user.cin}</strong></li>
                        <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Role : {props.user.role}</strong></li>
                        <li className="list-group-item mx-2 m-1"><strong style={{ fontFamily:'Arial',fontWeight:'800' }}>Date de creation de compte : {props.user.created_at}</strong></li>
                    </ul>
                    <button className='btn btn-success'>Modifier mon mot de passe</button>
                </div>
            </div>
        </div>
    </div>
  )
}
