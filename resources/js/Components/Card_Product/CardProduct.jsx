import React from 'react'
import ordi from '../../Images/ordi.jpeg';
import './CardProduct.css';

export default function CardProduct(props) {
  return (
    <div>
        <div className="card rounded-4 border-0 p-4 brradius shadow">
            <img src={ordi} className="card-img-top" alt=" 2" />
            <h4 className='titre-product'>Ordinateur hp</h4>
            <div className="card-body px-0 d-flex">
               <span className='stock px-2'><small>Stock:10</small></span>
               <span><button className='px-4 mx-4 btnDetail'>Detail</button></span>
            </div>
          </div>
    </div>
  )
}
