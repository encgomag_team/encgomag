import React from 'react';
import './HeaderPublic.css';
import LOGO_HEADER from '../../Images/logo-no-background 6.png';
import { Link, usePage } from '@inertiajs/react';

export default function HeaderPublic() {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <Link className="navbar-brand" href="/"><img src={LOGO_HEADER} height={30} width={230} alt="LOGO" title='ENCGOMAG' /></Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            </ul>
            {/* <button>fzdzd</button> */}
          </div>
        </div>
      </nav>

    </div>
  )
}
