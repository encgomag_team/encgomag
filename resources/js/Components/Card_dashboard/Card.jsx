import React from 'react'
import './Card.css';
import { Link } from '@inertiajs/react';

export default function Card(props) {
return (
<div>
    <div className="card rounded-4 border-0 shadow d-flex flex-row align-items-center">
        <div style={props.color} className="carre">
            <div className="image p-3">
                <img className='img-fluid'
                    src={props.image} alt="" />
            </div>
        </div>
        <div className='mx-3'>
            <h6 style={{ fontFamily:'Arial',fontWeight:'800',color:'#C85D22',boxShadow:'0px 2px 1px #C85D22' }} className='titre my-2'>{props.titre}</h6>
            <h1 style={{fontFamily:'Arial black',fontWeight:'900',color:'#C85D22',boxShadow:'0px 2px 1px #C85D22',textShadow:'0px 2px 1px #000000'}} className='fw-bolder text-center nombre'>{props.nombre}</h1>
           <Link href={props.route} style={{ backgroundColor:'#793D3D',color:"white",borderRadius:'20px',boxShadow:'0px 2px 1px #000000' }} className='btn mx-1 mb-2  text-center  btn-sm'>Voir plus</Link>
        </div>
    </div>
</div>
)
}
