import React from 'react'
import fourniture from '../../Images/fournitures.jpeg';
import './CardCategorie.css';
import { Link } from '@inertiajs/react';

export default function CardCategorie() {
  return (
    <div>
        <div>
            <Link className='categorie' href={route('fournitures')}>
                <div className="card border-0 rounded-4 p-4 brradius shadow">
                    <img src={fourniture} className="card-img-top"  alt=" 1" />
                    <div className="card-body ">
                        <Link className='btn btn-dark  btnnoir' href={route('fournitures')}><small>Electroniques</small></Link>
                    </div>
                </div>
            </Link>
        </div>
    </div>
  )
}
