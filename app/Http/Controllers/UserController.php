<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;

class UserController extends Controller
{
    public function login(Request $request){

        //Validation des entrées
        $request->validate([
            'email'=>['required'],
            'password'=>['required'],
        ],[
            'email.required'=>"L'email est obligatoire !",
            'password.required'=>"Le mot de passe est obligatoire !"
        ]);


        //Recuperation depuis le forumulaire
        $email=$request->email;
        $password=$request->password;

        //Logique de login
        if(Auth::attempt(['email' => $email, 'password' => $password])){
            return redirect(RouteServiceProvider::DASHBOARD);
            }else {
                return redirect('/user/login')->withErrors(['email' => 'Email ou mot de passe incorrect !'])->withInput($request->except('password'));
            }
        }

    public function logout(){
        Auth::logout();
        return redirect()->route('login')->withErros(['status'=>'Vous etes maintenant déconnecté !']);
    }
}

