<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Fourniture;

class Categorie extends Model
{
    use HasFactory;

    protected $fillable=[
        'titre',
    ];

    public function fournitures(){
        return $this->hasmay(Fourniture::class);
    }

}
