<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Fourniture;
use App\Models\User;

class Commande extends Model
{
    use HasFactory;

    protected $fillable=[
        'date_commande',
        'quantite',
        'status',
        'id_user',
        'id_fourniture',
    ];

    public function user(){
        return $this->belongsTo(User::class,'id_user');
    }

    public function fourniture(){
        return $this->belongsTo(Fourniture::class,'id_fourniture');
    }

}
