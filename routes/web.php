<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserController;
use Inertia\Inertia;
// use Illuminate\Http\Request;
// use Illuminate\Routing\UrlGenerator;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Home/Home');
});

//Routes AUthentification
        Route::get('/user/login',function(){
            return Inertia::render('Auth/Login');
        })->name('login');

        Route::post('/user/login',[UserController::class, 'login'])->name('login_traitement');

        Route::post('/web/user/logout',[UserController::class, 'logout'])->name('logout_traitement');

        Route::middleware(['auth_perso'])->group(function () {

            Route::get('/dashboard', function () {
                $user=Auth::user();
                if($user->role==='fonctionnaire'){
                    return Inertia::render('Fonctionnaire/Dashboard');
                }else if($user->role==='responsable'){
                    return Inertia::render('Responsable/Dashboard');
                }else if($user->role==='admin'){
                    return Inertia::render('Admin/Dashboard');
                }else{
                    return Inertia::render('Error_404/Error');
                };
            })->name('dashboard');
        });

//------------------------------------------------------

//Routes uniquement pour le responsable

    Route::middleware(['auth', 'responsable'])->group(function () {

        Route::get('/commandes',function(){
            return Inertia::render('Responsable/Commandes');
        })->name('commandes');

        //-----------------------//

        Route::get('/detail',function(){
            return Inertia::render('Responsable/DetailCommande');
        })->name('detail');

        Route::get('/profile',function(){
            return Inertia::render('Responsable/Profile');
        })->name('profile_responsable');

        Route::get('/fournitures',function(){
            return Inertia::render('Responsable/Fournitures');
        })->name('fournitures');

        Route::get('/categories',function(){
            return Inertia::render('Responsable/Categories');
        })->name('categories');

    });


//Routes uniquement pour l'admin

Route::middleware(['auth', 'admin'])->group(function () {

    Route::get('/users/list',function(){
        return Inertia::render('Admin/UsersList');
    })->name('users');

    Route::get('/users/addUser',function(){
        return Inertia::render('Admin/AddUser');
    })->name('ajouter_user');

    Route::get('/users/detail',function(){
        return Inertia::render('Admin/DetailUser');
    })->name('detail_user');

});


















    Route::get('/web/encgomag/app/error/access_denieded/Forbidden',function(){
        return Inertia::render('Forbidden_403/Forbidden');
    })->name('forbidden');




    // Route::get('/retour', function (Request $request, UrlGenerator $urlGenerator) {
    //     $lastRoute = $request->path();
    //     $lastRouteUrl = $urlGenerator->to($lastRoute);
    //     return Inertia::render('Dashboard', ['lastRouteUrl' => $lastRouteUrl]);
    // })->name('back');



//////////////////


    Route::get('/edit_user',function(){
        return Inertia::render('Admin/EditUser');
    });







//Route pour la page d'erreur ERROR_404
    Route::fallback(function(){
        return Inertia::render('Error_404/Error');
    });



    Route::get('/fonctionnaire/fournitures/categorie',function(){
        return Inertia::render('Fonctionnaire/CategoriesFonct');
    })->name('categories_fonct');

    Route::get('/fonctionnaire/fournitures/list',function(){
        return Inertia::render('Fonctionnaire/ListFournitures');
    })->name('liste_fournitures_fonctionnaire');

    Route::get('/fonctionnaire/fournitures/booking',function(){
        return Inertia::render('Fonctionnaire/FormDemande');
    })->name('fonctionnaire_demande');

    Route::get('/fonctionnaire/fournitures/suivre_demande',function(){
        return Inertia::render('Fonctionnaire/SuivreDemande');
    })->name('fonctionnaire_suivie');
